<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Works;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function ViewPosts(){
        $isNews = Works::where('works_category_id',1)->get();
        return view('news',compact('isNews'));
    }
    public function ReadBlog($id)
    {
        $readPosts = Blog::find($id);
        if($readPosts){
            return view('news',compact('readPosts'));
        }else{
            return back();
        }
    }
    public function ReadWorks($id)
    {
        $readWorks = Works::find($id);
        if($readWorks){
            return view('project',compact('readWorks'));
        }else{
            return back();
        }
    }
    public function Similar($id)
    {
        $blog = Blog::find($id);
        return view('news',[
            'blog' => $blog->load(['category','Similar.article']),
            'readPosts' =>  $blog,
            'last' => Blog::where('blog_category_id',$blog->blog_category_id)->orderBy('created_at','desc')->limit(3)->get()
        ]);

    }
}
