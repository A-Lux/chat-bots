<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Works;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function Main()
    {
        $isWork = Works::get();
        return view('index',compact('isWork'));
    }
    public function About()
    {
        return view('about-us');
    }
    public function Works()
    {
        $works = Works::with('category')->orderBy('id','desc')->paginate(6);
        $chatBots = Works::where('works_category_id',1)->paginate(6);
        $webSites = Works::where('works_category_id',2)->paginate(6);
        $mobile = Works::where('works_category_id',3)->paginate(6);
        $videosos = Works::where('works_category_id',4)->paginate(6);

        return view('work',compact('works','chatBots','webSites','mobile','videosos'));
    }
    public function Blog()
    {
        $blogs = Blog::with('category')->get();
        $blogNews = Blog::where('blog_category_id',1)->paginate(6);
        $blogArticle = Blog::where('blog_category_id',2)->paginate(6);
        $blogJob = Blog::where('blog_category_id',3)->paginate(6);
        return view('blog', compact('blogs','blogNews', 'blogArticle', 'blogJob'));
    }
    public function Contacts()
    {
        return view('contacts');
    }

    public function Project()
    {
        return view('project');
    }
}
