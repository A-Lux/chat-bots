<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function category()
    {
        return $this->belongsTo('App\BlogCategory','blog_category_id','id');
    }

    public function Similar()
    {
        return $this->hasMany('App\SimilarPosts','article_id','id');
    }
}
