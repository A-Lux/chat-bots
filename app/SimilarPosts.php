<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SimilarPosts extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'article_id',
        'similar_id'
        ];
    public function article()
    {
        return $this->hasOne('App\Blog','id','similar_id');
    }
}
