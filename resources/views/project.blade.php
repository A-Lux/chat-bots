<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>A-LUX Chat-bot-dev</title>
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="/css/style.min.css" />
      <script type="text/javascript" src="https://vk.com/js/api/share.js?95" charset="windows-1251"></script>
  </head>
  <body>
    <section class="active__header">
      <div class="active__menu active__menu-left">
        <a class="active__logo" href="{{route('main')}}"
          ><img src="/img/logo.svg" alt="logo"
        /></a>
        <div class="container">
          <div class="active__link">
            <a href="{{route('main')}}" class="close_navbar scroll-btn2">Главная</a>
            <a href="{{route('about')}}" class="close_navbar scroll-btn2">О компании</a>
            <a href="{{route('works')}}" class="close_navbar scroll-btn2">Наши Работы</a>
            <a href="{{route('blog')}}" class="close_navbar scroll-btn2">Блог</a>
            <a href="{{route('contacts')}}" class="close_navbar scroll-btn2">Контакты</a>
            <a href="tel:+77273171698" class="active-border">+7 727 317-16-98</a>
            <a href="tel:+77054503916" class="active__num active-border">+7 705 450-39-16</a>
          </div>
        </div>
      </div>
    </section>
    <header class="header">
      <div class="container">
        <div class="header__inner">
          <a href="{{route('main')}}" class="header__logo">
            <img src="/img/logo.svg" alt="" />
          </a>
          <nav class="header__navbar">
            <a href="{{route('main')}}" class="header__link header__link_active">Главная</a>
            <a href="{{route('about')}}" class="header__link header__link_active">О КОМПАНИИ</a>
            <a href="{{route('works')}}" class="header__link header__link_active">Наши работы</a>
            <a href="{{route('blog')}}" class="header__link header__link_active">блог</a>
            <a href="{{route('contacts')}}" class="header__link header__link_active">контакты</a>
          </nav>
            <div class="header__wrap">
                <button class="header__button popup-open">Заказать услугу</button>
                <div class="header__phones">
                    <a href="" class="header__phone"><img src="/img/main-page/header-phone.svg" alt="" /> +7 727 317 16 98</a>
                    <a href="" class="header__phone"><img src="/img/main-page/header-phone.svg" alt="" /> +7 705 450 39 16</a>
                </div>
            </div>
          <div class="hamburger">
            <span></span>
          </div>
        </div>
      </div>
    </header>
    <section class="blog-main">
      <div class="container"></div>
    </section>
    <section class="project">
      <div class="container">
        <div class="project__inner">
          <div class="project__date">{!! $readWorks->created_at->day.'.'.$readWorks->created_at->month.'.'.$readWorks->created_at->year!!}</div>
          <div class="title">{!! $readWorks->name !!}</div>
          <div class="row">
            <div class="project__wrap col-lg-4 col-md-5">
              <div class="project__title">Задача</div>
              <div class="project__descr">
                  {!! $readWorks->task !!}
              </div>
            </div>
            <div
              class="project__wrap offset-lg-1 col-lg-4 offset-md-1 col-md-5"
            >
              <div class="project__title">Решение</div>
              <div class="project__descr">
                {!! $readWorks->solution !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="projectImg">
      <div class="projectImg__bg1">
        <img src="/img/project-page/bg-up.svg" alt="" />
      </div>
      <div class="projectImg__bg2">
        <img src="/img/project-page/bg-down.svg" alt="" />
      </div>
      <div class="container">
        <div class="projectImg__inner">
          <div class="projectImg__wrap">
            <div class="projectImg__circle">
              <img src="/img/main-page/services-circle.png" alt="" />
            </div>
            <div class="projectImg__img">
                <img src="{!! asset('storage/'.$readWorks->image) !!}" style="width: 100%">
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="socials">
      <div class="container">
        <div class="socials__inner">
          <div class="socials__title">Поделиться с помощью:</div>
          <div class="socials__wrap">
              <a href="" class="socials__link">
              {{--                    <img src="/img/news-page/icon1.svg" alt=""/>--}}
              <!-- Put this script tag to the place, where the Share button will be -->
                  <script type="text/javascript"><!--
                      document.write(VK.Share.button(false, {
                          type: "custom",
                          text: "<img src=\"/img/news-page/icon1.svg\"   />"
                      }));
                      --></script>
              </a>
              <a href="https://www.instagram.com/aluxkz/" class="socials__link">
                  <img src="/img/news-page/icon2.svg" alt=""/>
              </a>
              <a href="" class="socials__link">
                  {{--                    <img src="/img/news-page/icon3.svg" alt=""/>--}}
                  <script async src="https://telegram.org/js/telegram-widget.js?14" data-telegram-share-url="https://core.telegram.org/widgets/share" data-size="large" data-text="notext"></script>
              </a>
              <a href="" id="ok_shareWidget" class="socials__link">
                  <img  src="/img/news-page/icon4.svg" alt=""/>
              </a>
              <a href="" class="socials__link">
                  <img src="/img/news-page/icon5.svg" alt=""/>
              </a>
              <a href="" class="socials__link">
                  <img src="/img/news-page/icon6.svg" alt=""/>
              </a>
              <a href="" class="socials__link">
                  <img src="/img/news-page/icon7.svg" alt=""/>
              </a>
          </div>
        </div>
      </div>
    </section>
    <section class="feedback" style="background-color: white">
      <div class="container">
        <div class="feedback__inner">
          <div class="feedback__wrap offset-lg-6 col-lg-4 offset-md-5 col-md-7">
            <div class="feedback__title">Обратная связь</div>
            <div class="feedback__subtitle">
              Будем рады ответить на любой ваш вопрос по телефону
            </div>
            <form class="form" action="{{route('request')}}" method="post">
                {{csrf_field()}}
              <input class="input" type="text" placeholder="Ваше имя" name="name" id="name"/>
              <input class="input" type="text" placeholder="Номер телефона" name="phone_number" id="phone_number"/>
              <button class="button btn_active">Перезвоните мне</button>
              <div class="form__subtitle">
                Нажимая на кнопку вы соглашаетесь на обработку персональных
                данных
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="container">
        <div class="footer__inner">
          <div class="row">
            <div class="col-md-4">
              <div class="footer__description">
                COPYRIGHT © 2007-2020 ALMATY LUXURY CHAT BOT DEV. Создание
                чат-ботов в Алматы
              </div>
              <a href="" class="footer__chatBot"
                >2020 — <span>chat-bots.kz</span></a
              >
            </div>
            <div class="col-md-5 footer__wrap">
              <div class="footer__title">Все страницы:</div>
              <div class="footer__row">
                <a href="{{route('main')}}" class="footer__link">Главная</a>
                <a href="{{route('about')}}" class="footer__link">О компании</a>
                <a href="{{route('works')}}" class="footer__link">Наши работы</a>
                <a href="{{route('blog')}}" class="footer__link">Блог</a>
                <a href="{{route('contacts')}}" class="footer__link">Контакты</a>
              </div>
            </div>
            <div class="col-md-3 footer__wrap2">
              <div class="footer__title">Наши телефоны:</div>
              <div class="footer__row">
                <a href="tel:77273171698" class="footer__link">+7 727 317 16 98</a>
                <a href="tel:77054503916" class="footer__link">+7 705 450 39 16</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <div class="popup-fade">
        <div class="popup">
            <form class="form" action="{{route('request')}}" method="post">
                {{csrf_field()}}
                <a class="popup-close" href="#">&times;</a>
                <input class="input" type="text" placeholder="Ваше имя" name="name" id="name" />
                <input class="input" type="text" placeholder="Номер телефона" name="phone_number" id="phone_number"/>
                <button class="button btn_active">Перезвоните мне</button>
                <div class="form__subtitle">
                    Нажимая на кнопку вы соглашаетесь на обработку персональных данных
                </div>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/libs.min.js"></script>
    <script src="/js/main.js"></script>
    <script>
        !function (d, id, did, st, title, description, image) {
            function init(){
                OK.CONNECT.insertShareWidget(id,did,st, title, description, image);
            }
            if (!window.OK || !OK.CONNECT || !OK.CONNECT.insertShareWidget) {
                var js = d.createElement("script");
                js.src = "https://connect.ok.ru/connect.js";
                js.onload = js.onreadystatechange = function () {
                    if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                        if (!this.executed) {
                            this.executed = true;
                            setTimeout(init, 0);
                        }
                    }};
                d.documentElement.appendChild(js);
            } else {
                init();
            }
        }(document,"ok_shareWidget",document.URL,'{"sz":30,"st":"rounded","nc":1,"ck":2,"bgclr":"ED8207","txclr":"FFFFFF"}',"","","");
    </script>
  </body>
</html>
